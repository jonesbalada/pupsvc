const express = require('express')
const app = express()
const port = 3300
const puppeteer = require('puppeteer');

app.get('/pdf', (req, res) => {
    (async () => {
        const html = req.query.html;
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        page.setContent('<html><body>html content: ' + html + '</body></html>')
        const buffer = await page.pdf({format: 'A4'});

        res.type('application/pdf')
        res.send(buffer)

        await browser.close();
    })();
})

app.listen(port, () => console.log(`
Example app listening on port ${port}! use 'http://localhost:3300/pdf?html=hello'
`))
